import setuptools

setuptools.setup(
	name='colored-string',
	version='0.1',
	author='Marc Wolff',
	author_email='mwolff.mw@gmail.com',
	description='A library to facilitate the creation and manipulation of colored strings',
	url='https://github.com/wolffmarc/colored-string',
	packages=setuptools.find_packages()
)
